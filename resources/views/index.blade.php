@extends('layouts.default')

@section('content')
    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-B">
                    <h1>Mahasiswa</h1>
                    <a href="{{url('create')}}" class="btn btn-danger"> Tambah Mahasiswa </a>
                </div>

                <div class="col-lg-B mt-5">
                    <table class="table table-dark">
                        <tr>
                            <th>Id</th>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($data as $dataMahasiswa)
                            <tr>
                                <td>{{ $dataMahasiswa->id }}</td>
                                <td>{{ $dataMahasiswa->nama_mahasiswa }}</td>
                                <td>{{ $dataMahasiswa->nim_mahasiswa }}</td>
                                <td>{{ $dataMahasiswa->kelas_mahasiswa }}</td>
                                <td>{{ $dataMahasiswa->prodi_mahasiswa }}</td>
                                <td>{{ $dataMahasiswa->fakultas_mahasiswa }}</td>
                                <td>
                                    <a href="{{ url('/show/'.$dataMahasiswa->id) }}" class="btn btn-warning">Edit</a>
                                    <a href="{{ url('/destroy/'.$dataMahasiswa->id) }}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection