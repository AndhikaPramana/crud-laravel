@extends('layouts.default')

@section('content')
    <section>
        <div class="container mt-5">
            <h1>Tambah Mahasiswa</h1>
            <div class="row">
                <div class="col-lg-B">
                    <form action="{{ url('/store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Mahasiswa</label>
                            <input type="text" name="nama_mahasiswa" class="form-control" placeholder="Nama Lengkap Mahasiswa">
                        </div>
                        <div class="form-group">
                            <label for="nim">NIM Mahasiswa</label>
                            <input type="number" name="nim_mahasiswa" class="form-control" placeholder="NIM Mahasiswa">
                        </div>
                        <div class="form-group">
                            <label for="kelas">Kelas Mahasiswa</label>
                            <input type="text" name="kelas_mahasiswa" class="form-control" placeholder="Kelas Mahasiswa">
                        </div>
                        <div class="form-group">
                            <label for="prodi">Prodi Mahasiswa</label>
                            <input type="text" name="prodi_mahasiswa" class="form-control" placeholder="Prodi Mahasiswa">
                        </div>
                        <div class="form-group">
                            <label for="fakultas">Fakultas Mahasiswa</label>
                        </div>
                        <input type="text" name="fakultas_mahasiswa" class="form-control" placeholder="Fakultas Mahasiswa">
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-danger">Tambah</button>
                        </div>
                        <div class="form-group mt-2">
                            <a href="{{ url('/') }}">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection